<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControlPanel extends CI_Controller{
  public function __construct(){
    parent::__construct();
    // $this->load->library('form_validation');
    $this->load->model("HeroModel", "heroModel");
  }
  
  public function index(){
    //  $this->debugMode(__FILE__, __FUNCTION__);
    // Ini hanya wrapper ke hero
    $this->hero();
  }
  
  public function heroList(){
    $this->debugMode(__FILE__, __FUNCTION__);
    $data['heroList'] = $this->heroModel->getHero();
    
    $this->load->view('controlPanel/template/header', $data);
    $this->load->view('controlPanel/template/sidebar', $data);
    $this->load->view('controlPanel/hero/heroList', $data);
    $this->load->view('controlPanel/template/footer', $data);
    
  }
  
  public function hero(){
    $this->debugMode(__FILE__, __FUNCTION__);
    
    $data['title'] = 'Admin Hero';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->load->view('controlPanel/template/header', $data);
    $this->load->view('controlPanel/template/sidebar', $data);
    $this->load->view('controlPanel/hero/addHero', $data);
    $this->load->view('controlPanel/template/footer', $data);
  }

  public function addHero(){
    $label = $this->input->post('label');
    $deskripsi = $this->input->post('deskripsi');
    // $this->load->model('HeroModel');

    $config['upload_path']          = BASEPATH . "./../uploads";
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 100000;
    $config['max_width']            = 4000;
    $config['max_height']           = 4000;
    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('gambar')) {
      $error = array('error' => $this->upload->display_errors());
      echo "gagal";
    } else {
      $data = array('upload_data' => $this->upload->data());
      echo "sukses";
      $this->heroModel->insertHero(
        $label,
        $deskripsi,
        $data['upload_data']['file_name'],
        'belum disetujui'
      );
    }
    // die;
    redirect("ControlPanel/index");
  }

  private function debugMode($fileName, $function){
    echo "$fileName"
        ."@$function";
  }


  // public function brownies()
  // {
  //     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
  //     $this->cekLogin();
  //     $this->load->view('catalog/brownis', $data);
  // }
  // public function brownies()
  // {
  //     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
  //     $this->cekLogin();
  //     $this->load->view('catalog/brownis', $data);
  // }

  // public function desert()
  // {
  //     $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
  //     $this->cekLogin();
  //     $this->load->view('catalog/dessert', $data);
  // }

  // private function cekLogin()
  // {
  //     if ($this->session->role_id == "" || $this->session->email == "") {
  //         redirect('auth');
  //     }
  // }
}
